﻿<?php 
    $mpiLimit = 3;
    $mpiMatches = array();

    $commonKeyWords = ['a','ante','até','após','de','desde','em','entre','com','para','por','perante','sem','sob','sobre','na','no','e','do','da', 'mais', 'empresa', 'empresas', 'servico', 'servicos', 'fabricacao', 'fabricante', 'distribuidor', 'distribuidores', 'fornecedor', 'fornecedores', 'aluguel', 'preco', 'valor', 'orcamento', 'projeto', 'projetos', 'frete', 'calcular', 'calculo', 'rápido', 'brasil', 'sao paulo', 'sp', 'entrega', 'cotação', 'consulta', 'cotar', 'enviar', 'envio','preço', 'compra', 'venda', 'produtor', 'marca', 'garantia','tipos', 'especialização', 'recomendações', 'eficiência', 'durabilidade','uso', 'benefícios'];

    $currentKeyWords = array_diff(explode('-', $urlPagina), $commonKeyWords);

    foreach($vetKey as $key => $item): 
        $mpiKeyWords = array_diff(explode('-', $item['url']), $commonKeyWords);
        $mpiEqualWords = count(array_intersect($currentKeyWords, $mpiKeyWords));
        if($mpiEqualWords > 0 && $item['url'] != $urlPagina):
            array_push($mpiMatches, array("equalWords" => $mpiEqualWords, 'mpiKey' => $key));
        endif;
    endforeach;

    rsort($mpiMatches);

    if(count($vetKey) >= 5):
        while(count($mpiMatches) < $mpiLimit) {
            $randomRelated = array_rand($vetKey);
            if($urlPagina != $vetKey[$randomRelated]['url']):
                if(!in_array($randomRelated, array_column($mpiMatches, 'mpiKey'))):
                    array_push($mpiMatches, array('equalWords' => 0, 'mpiKey' => $randomRelated));
                endif;
            endif;        
        }
    endif;     
?>

<div class="mpi-related">
    <h2 class="fs-28 text-center">Páginas relacionadas</h2>
    <div class="row">
        <?php foreach($mpiMatches as $key => $item): 
        if($key >= $mpiLimit) break;
            $mpi = $vetKey[$item['mpiKey']];
            $mpiTitle = $mpi['key'];
            $mpiUrl = $mpi['url']; ?>
            <div class="p-2 col-4">
                <a rel="nofollow" href="<?=$url.$mpi['url']?>" title="<?=$mpiTitle?>">
                    <img class="mpi-related__cover" src="<?=$url?>imagens/informacoes/<?=$mpiUrl?>-01.jpg" title="<?=$mpiTitle?>" alt="<?=$mpiTitle?>" loading="lazy">
                    <h2 class="mpi-related__title"><?=$mpiTitle?></h2>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>