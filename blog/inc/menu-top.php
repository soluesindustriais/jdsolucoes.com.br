<?php foreach ($menu as $key => $value): ?>
    <? if($sigMenuPosition !== false && $key == $sigMenuPosition) include 'inc/menu-top-inc.php'; ?>
    <li <?=($value[2] ? ' class="dropdown"' : '')?> <?=($value[3] ? ' data-icon-menu' : '')?>>
        <a href="<?=(strpos($value[0], 'http') !== false ? $value[0] : ($value[0] === '' ? $url.$value[0] : $urlBase.$value[0]))?>" title="<?=($value[1] == 'Home' ? 'Página inicial' : $value[1])?>" <?=(strpos($value[0], 'http') !== false ? 'target="_blank" rel="nofollow"' : "")?> <?= (strpos($value[0], '.pdf') !== false ? 'target="_blank"' : "")?>>
            <?php if(!$isMobile && $value[3]): 
                    if($value[0] !== 'informacoes'): ?>
                        <i class="<?=$value[3]?>"></i>
                        <span class="d-block"><?=$value[1]?></span>
                    <?php else: ?>
                        <i class="<?=$value[3]?> fa-xl"></i>
                    <?php endif; ?>
            <?php else: ?>
                <?=$value[1];?>
            <?php endif; ?>
        </a>
        <?php if($value[2]): 
            if($value[0] == 'informacoes'):
                echo '<ul class="sub-menu-info">';
                include('inc/'.$value[2].'.php');
            else:
                echo '<ul class="sub-menu">';
                $urlTemp = $url;
                $url = $urlBase;
                include($DIR.'inc/'.$value[2].'.php');
                $url = $urlTemp;
            endif;
            ?>
        </ul>
    <?php endif; ?>
</li>
<?php 
endforeach;
?>
