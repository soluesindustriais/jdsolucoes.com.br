<!--Modal-->
<div class="j_fundoModal none" id="down_control">
  <div class="j_conteudoModal">
    <form method="post" enctype="multipart/form-data" class="j_sendForm">
      <div class="btn-group pull-right">      
        <button type="submit" name="Cadastrar" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <button type="button" class="btn btn-danger j_fechar" id="down_control"><i class="fa fa-close"></i></button>      
        <input type="hidden" name="action" value="CadastraDown"/>
        <input type="hidden" name="baseDir" value="../../uploads/"/>
      </div>
      <div class="clearfix"></div>

      <div class="x_title">            
        <h2>Enviar arquivo relacionado</h2> 
        <div class="clearfix"></div>
      </div>      
      <div class="clearfix"></div>
      <br/>
      <div class="form-group">
        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--inclusão das seleções de categorias-->
          <?php include("inc/categorias.inc.php"); ?>
          <!-- /inclusão das seleções de categorias-->
        </div>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="form-group">
        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="dow_file">Arquivo <span class="text-info">(.pdf, .doc, .xlsx, .csv)</span> <span class="required">*</span>
        </label>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <input type="file" id="app_file" name="dow_file" required="required">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="dow_title">Nome <span class="required">*</span>
        </label>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <input type="text" id="dow_title" name="dow_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
          if (isset($post['dow_title'])): echo $post['dow_title'];
          endif;
          ?>">
        </div>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="form-group">
        <label class="control-label col-md-12 col-sm-12 col-xs-12" for="dow_description">Breve descrição <span class="required">*</span>
        </label>
        <div class="col-md-12 col-sm-12 col-xs-12">
          <textarea name="dow_description" required="required" class="form-control col-md-12 col-xs-12"><?php
            if (isset($post['dow_description'])): echo $post['dow_description'];
            endif;
            ?></textarea>
        </div>
      </div>

      <div class="clearfix"></div>
      <br/>
    </form>
  </div>
</div>
<!--/Modal-->