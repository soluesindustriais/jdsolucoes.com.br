<?
 $h1         = 'Mapa do Site';
 $title      = 'Mapa do Site';
 $desc       = 'Saiba tudo sobre o melhor portal B2B do Brasil';
 $key        = '';
 include( "inc/head.php" ); 
 ?>
<? include('inc/header.php'); ?>

<div class="container my-5 py-5">
    <h2 class="text-uppercase my-2 p-3 btn-primary text-white">Produtos</h2>
    <ul class="list-group p-5 w-50 list-map">
        <?
            foreach($categorias->getCategorias() as $categoria)
            {
                $categoriaSemAcento = $trata->trataAcentos($categoria);
                echo "<li class=\"list-group-item active text-uppercase\">".$trata->retiraHifen($categoria)."</li>";
                include("inc/".$categoriaSemAcento."/".$categoriaSemAcento."-sub-menu.php"); 
            };
        ?>
    </ul>
</div>




 <script>
    const listItem = document.querySelectorAll(".list-group > li");
    const addClassList = (item) => 
    {
        item.classList.add("list-group-item");
    };

    for (let i = 0; i < listItem.length; i++) 
    {
        addClassList(listItem[i]);
    };
</script>

<? include('inc/footer.php'); ?>