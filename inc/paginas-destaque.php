
  <div class="container my-5">
 <h2 class="pb-3">Páginas em destaque</h2>   
    <div class="position-relative">
      <div class="row flex-nowrap content-wrapper">
        <div class="col-12 col-lg-4 p-0">
          <div class="content-block">
                <div class="col-12 pb-2">
                        <div class="card blog rounded border-0 shadow overflow-hidden">
                            <div class="position-relative border-intro">
                                <img src="imagens/logo.logo-solucs.webp" class="card-img-top" alt="...">
                                <div class="overlay rounded-top bg-dark"></div>
                            </div>
                            <div class="card-body content border-intro">
                                <h5><a href="javascript:void(0)" class="card-title title text-dark">Palavra-chave 3</a></h5>
                                <div class="post-meta d-flex justify-content-between mt-3">
                                    <a href="page-blog-detail.html" class="text-muted readmore">Read More <i class="mdi mdi-chevron-right"></i></a>
                                </div>
                            </div>
                            <div class="author">
                                <small class="text-light user d-block"><i class="mdi mdi-account"></i> categoria</small>
                                <small class="text-light date"><i class="mdi mdi-calendar-check"></i> palavra-chave</small>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-12 col-lg-4 p-0">
          <div class="content-block">
                <div class="col-12 pb-2">
                        <div class="card blog rounded border-0 shadow overflow-hidden">
                            <div class="position-relative border-intro">
                                <img src="imagens/logo.logo-solucs.webp" class="card-img-top" alt="...">
                                <div class="overlay rounded-top bg-dark"></div>
                            </div>
                            <div class="card-body content border-intro">
                                <h5><a href="javascript:void(0)" class="card-title title text-dark">Palavra-chave 3</a></h5>
                                <div class="post-meta d-flex justify-content-between mt-3">
                                    <a href="page-blog-detail.html" class="text-muted readmore">Read More <i class="mdi mdi-chevron-right"></i></a>
                                </div>
                            </div>
                            <div class="author">
                                <small class="text-light user d-block"><i class="mdi mdi-account"></i> categoria</small>
                                <small class="text-light date"><i class="mdi mdi-calendar-check"></i> palavra-chave</small>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-12 col-lg-4 p-0">
          <div class="content-block">
                <div class="col-12 pb-2">
                        <div class="card blog rounded border-0 shadow overflow-hidden">
                            <div class="position-relative border-intro">
                                <img src="imagens/logo.logo-solucs.webp" class="card-img-top" alt="...">
                                <div class="overlay rounded-top bg-dark"></div>
                            </div>
                            <div class="card-body content border-intro">
                                <h5><a href="javascript:void(0)" class="card-title title text-dark">Palavra-chave 3</a></h5>
                                <div class="post-meta d-flex justify-content-between mt-3">
                                    <a href="page-blog-detail.html" class="text-muted readmore">Read More <i class="mdi mdi-chevron-right"></i></a>
                                </div>
                            </div>
                            <div class="author">
                                <small class="text-light user d-block"><i class="mdi mdi-account"></i> categoria</small>
                                <small class="text-light date"><i class="mdi mdi-calendar-check"></i> palavra-chave</small>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
      </div>
      <div class="controls d-flex d-lg-none justify-content-between align-items-center px-1">
        <span class="btn-slider"  id="prev"><</span>
        <span class="btn-slider"  id="next">></span>
      </div>
    </div>
  </div>
 
 

<script>
    
    let page = 0;

const wrapper = document.querySelector(".content-wrapper");
const nextButton = document.querySelector("#next");
const prevButton = document.querySelector("#prev");

function slideTo(forward) {
  const scrollStep = wrapper
    .querySelector(".content-block")
    .parentElement.getBoundingClientRect().width;

  const scrollMax = [...wrapper.querySelectorAll(".content-block")]
    .map(el => el.parentElement.getBoundingClientRect().width)
    .reduce((i, c) => i + c);

  const new_page = page + (forward ? 1 : -1) * (forward === null ? 0 : 1);
  const scroll = new_page * scrollStep;

  if (scroll >= 0 && scroll < scrollMax) {
    page = new_page;
    wrapper.scrollLeft = scroll;
  }
}

nextButton.addEventListener("click", () => slideTo(true));
prevButton.addEventListener("click", () => slideTo(false));
window.addEventListener("resize", () => slideTo(null));

</script>