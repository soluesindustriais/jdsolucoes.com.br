<div class="mw-100">
        <form class="d-flex justify-content-center mw-75 w-75 mx-auto" action="busca.php" method="get">
                <input id="searchBox" name="produto" type="text" class="form-control mw-100" style="height:3em">
                <button name="search" type="submit" class="btn btn-primary mw-25" style="font-size: 1.1rem;"><i class="fas fa-search d-inline mx-1"></i></button>
        </form>
</div>