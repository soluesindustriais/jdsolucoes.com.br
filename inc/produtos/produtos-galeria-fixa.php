
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<div class="container p-0 my-3"><div class="row no-gutters justify-content-center py-3"><div class="col-lg-12 pt-2 mt-2 text-center"><div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag"><div class="col-12 py-2"><div class="owl-stage-outer"><div class="owl-stage"> <?php $lista = array('Modem wifi para van','Comprar aparelho de wifi para micro ônibus executivo','Roteador para IOT para micro ônibus SP','Aparelho de wifi para micro ônibus','Aparelho de wifi para micro ônibus SP','Comprar aparelho de wifi para micro ônibus','Roteador para iot para micro ônibus executivo','Roteador para IOT para micro ônibus','Comprar aparelho de wifi para micro ônibus SP','Roteador para IOT para van','Roteador para IOT','Roteador wifi para IOT','Assistência técnica para wi fi para micro ônibus executivo SP','Onde comprar roteador para IOT','Assistência técnica para roteador para micro ônibus executivo','Assistência técnica para wi fi para micro ônibus executivo','Roteador para automação','Roteador para automação SP','Roteador para automação industrial em SP','Roteador wifi para automação industrial','Roteador de automação','Roteador para automação industrial preço','Roteador para automação industrial valor','Roteador wifi para automação industrial preço','Roteador para CLP WEG','Roteador para CLP Schneider','Roteador para CLP Siemens','Aparelho roteador wifi para CLP','Instalação de roteador para micro onibus preço','Instalação de wi fi para micro ônibus executivo','Instalação de roteador para micro ônibus executivo','Instalação de wi fi para ônibus','Instalação de roteador para ônibus','Serviço de instalação de wi fi para ônibus','Instalação de wi fi para micro ônibus','Instalação de roteador para micro ônibus','Instalação de roteador para ônibus executivo','Instalação de wi fi para van','Instalação de roteador para van','Serviço de instalação de wi fi para van','Instalação de wifi para van','Instalação de roteador de van','Instalação de wi fi para van empresarial','Instalação de roteador para van empresarial','Serviço de instalação de wi fi para van empresarial','Instalação de roteador para ônibus preço','Empresa de wifi para ônibus SP','Empresa de manutenção de wi fi para ônibus executivo','Empresa de wifi para ônibus executivo','Empresa de manutenção de wi fi para ônibus','Empresa de internet wi fi para ônibus','Empresa de manutenção de wi fi para micro ônibus','Empresa de internet wi fi para van','Empresa de internet wi fi para van executiva','Fornecedor de modem wifi para van','Fornecedor de modem wifi para van executiva','Fornecedor de roteador para IOT','Fabricante de roteador para IOT','Fornecedor de wifi para micro ônibus','Fornecedor de wifi para onibus','Fornecedor de modem wifi para o ônibus','Fornecedor de wifi para micro ônibus executivo','Empresa de wifi para micro ônibus SP'); shuffle($lista); for($i=1;$i<13;$i++){ ?> <div class="owl-item"><div class="card blog rounded border-0 shadow overflow-hidden"> <div class="position-relative border-intro"><a class="lightbox" href="<?=$url;?>imagens/produtos/produtos-<?=$i?>.webp" title="<?=$lista[$i]?>"><img src="<?=$url;?>imagens/produtos/thumbs/produtos-<?=$i?>.webp" alt="<?=$lista[$i]?>" title="<?=$lista[$i]?>"/><div class="overlay rounded-top bg-dark"></div><div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?=$lista[$i]?></small></div> </div></div></a></div><?php } ?></div></div></div></div></div></div></div>
<?php
$folder = "produtos";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/produtos/produtos-" . $i . ".webp",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-29"
];

?>


<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>