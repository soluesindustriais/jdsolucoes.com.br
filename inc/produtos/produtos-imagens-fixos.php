<div class="col-12 pt-4"><div class="jumbotron py-1"><div class="row"><div class="col-lg-6 col-md-6 col-sm-12 py-lg-4 py-3 text-center"><a href="<?=$url?>imagens/produtos/produtos-01.webp"class="lightbox" title="<?=$h1?>"><img src="<?=$url?>imagens/produtos/thumbs/produtos-01.webp"class="img-thumbnail mb-1 lazy" alt="<?=$h1?>" title="<?=$h1?>" /></a><strong>Imagem ilustrativa de <?=$h1?></strong></div><div class="col-lg-6 col-md-6 col-sm-12 py-lg-4 py-3 text-center"><a href="<?=$url?>imagens/produtos/produtos-02.webp" title="<?=$h1?>" target="_blank"><img src="<?=$url?>imagens/produtos/thumbs/produtos-02.webp"class="img-thumbnail mb-1 lazy" alt="<?=$h1?>" title="<?=$h1?>" /></a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div></div></div>
                    <script type="application/ld+json">
                    {
                        "@context": "https://schema.org",
                        "@type": "ItemList",
                        "itemListElement": [{
                                "@type": "ImageObject",
                                "author": "Soluções Industriais",
                                "contentUrl": "<?= $url ?>imagens/produtos/thumbs/produtos-01.webp",
                                "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto.",
                                "name": "<?= $h1 ?> modelo 01",
                                "uploadDate": "2024-02-29"
                            },
                            {
                                "@type": "ImageObject",
                                "author": "Soluções Industriais",
                                "contentUrl": "<?= $url ?>imagens/produtos/thumbs/produtos-02.webp",
                                "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto.",
                                "name": "<?= $h1 ?> modelo 02",
                                "uploadDate": "2024-02-29"
                            }
                        ]
                    }
                    </script>
                    